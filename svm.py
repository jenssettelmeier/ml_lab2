# import files
from kernels import Kernels
import plot
# import packages
from scipy . optimize import minimize
import matplotlib . pyplot as plt
import numpy as np
import random, math

# objective is a function you have to define, which

# variables
# t =
# K =
# C =

def objective(a):
    """
    Equation 4
    takes a vector a as argument and returns a scalar value
    effectively implementing the expression that should be minimized
    """

    sum1 = 0
    for i in range(len(a)):
        sum2 = 0
        for j in range(len(a)):
            sum2 += a[i] * a[j] * t[i] * t[j] * K

        sum1 += sum2

    result = sum1 * 0.5

    sum3 = 0
    for i in range(len(a)):
        sum3 += a[i]

    result = result - sum3

    return result

    # when should we subtract the third sum?


def indicator():
    """
    Equation 6
    """
    


def calc_b():
    """
    Equation 7
    """
    b = 0
    for i in range(len(a)):
        b += a[i] * t[i] * K - t


def zerofun(a):
    """
    Equation 10
    """
    for i in range(len(a)):
        if (a[i] <= C):




def data():
    """
    Generate random data
    """
    # if you want the same "random" data each time use np.random.seed(14)
    classA = np.concatenate(np.random.randn(10, 2)....)
    classB =

    inputs =
    targets =

    N =

    permute =
    random.shuffle(permute)
    inputs =
    targets =


# start is a vector with the initial guess of the ⃗α vector.
#We can, e.g., simply use a vector of zeros: numpy.zeros(N).
#N is here the number of training samples (note that each training sample will have a corresponding α-value).
start = np.zeros(len(a))

# B is a list of pairs of the same length as the ⃗α-vector, stating the lower
# and upper bounds for the corresponding element in ⃗α.
# To constrain the α values to be in the range 0 ≤ α ≤ C,
# we can set bounds=[(0, C) for b in range(N)].


# This will find the vector ⃗α which minimizes the function objective
# within the bounds B and the constraints XC.
ret = minimize ( objective , start , bounds=B, constraints=XC )
alpha = ret [ ’x ’ ]  # There are other useful indices that you can use;
# in particular, the index 'success' holds a boolean value which is
# True if the optimizer actually found a solution.

# To only have a lower bound, set the upper bound to None like this: bounds=[(0, None) for b in range(N)].

"""
XC is used to impose other constraints, in addition to the bounds. We will
use this to impose the equality constraint, that is, the second half of (10).
This parameter is given as a dictionary with the fields type and fun, stating
the type of the constraint and its implementation, respectively. You can write
an equality constraint like this: constraint={'type':'eq', 'fun':zerofun},
where zerofun is a function you have defined which calculates the value which
should be constrained to zero. Like objective, zerofun takes a vector as
argument and returns a scalar value.
"""
